package snake.display.keyboard;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class SnakeKeyListener implements KeyListener {

	public HandleKey handler = new HandleKey();

	public SnakeKeyListener(int code){
		handler.direction=code;
	}
	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyPressed(KeyEvent e) // 按下键盘方向键
	{
		int d=handler.direction;
		if(d==-1)return;
		int code = e.getKeyCode();
		if(Math.abs(d - code) != 2)
			handler.direction = code;
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	public class HandleKey{
		public int direction=-1;
	}
}
