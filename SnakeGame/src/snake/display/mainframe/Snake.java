package snake.display.mainframe;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Panel;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import snake.display.keyboard.SnakeKeyListener;
import snake.entity.Body;
import snake.entity.Item;

class MyPanel extends Panel implements Runnable// 自定义面板类，继承了键盘和线程接口
{
	/**
	 *蛇实体，包括设置蛇体位置，判断吃到新的身体
	 */
	private static final long serialVersionUID = 1L;
	SnakeKeyListener skl;// 默认方向
	SnakeKeyListener.HandleKey handler;
	ArrayList<Body> snake; // 定义蛇按钮
	int shu; // 蛇的节数
	Item food;
	boolean result; // 判定结果是输 还是赢
	Thread thread; // 定义线程
	int weix, weiy; // 食物位置
	boolean t=true; // 判定游戏是否结束
	int fangxiang; // 蛇移动方向
	int x, y; // 蛇头位置

	MyPanel() {
		setLayout(null);
		setBackground(Color.cyan);
		skl = new SnakeKeyListener(KeyEvent.VK_RIGHT);
		handler = skl.handler;
		snake = new ArrayList<Body>();
		int	seed = (int) (Math.random() * 99);
		weix = (int) (seed * 0.1) * 60;
		weiy = (int) (seed % 10) * 40;
		food=new Body();
		add(food);
	}

	public void run() // 接收线程
	{

		while (t) {
			fangxiang = handler.direction;
			try {
				switch (fangxiang) {
				case KeyEvent.VK_RIGHT:
					x += 10;
					break;
				case KeyEvent.VK_LEFT:
					x -= 10;
					break;
				case KeyEvent.VK_UP:
					y -= 10;
					break;
				case KeyEvent.VK_DOWN:
					y += 10;
					break;
				}
			} catch (Exception e) {
			} finally {
				try {
					setLoc(x, y);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			int num1 = snake.size() - 1;
			while (num1 > 1)// 判断是否咬自己的尾巴
			{
				if (snake.get(num1).getBounds().x == snake.get(0).getBounds().x
						&& snake.get(num1).getBounds().y == snake.get(0)
								.getBounds().y) {
					t = false;
					result = false;
					repaint();
				}
				num1--;
			}
			if (x < 0 || x >= this.getWidth() || y < 0 || y >= this.getHeight())// 判断是否撞墙
			{
				t = false;
				result = false;
				repaint();
			}
			int num = snake.size() - 1;
			while (num > 0) // 设置蛇节位置
			{
				snake.get(num).setBounds(snake.get(num - 1).getBounds());
				num--;
			}

			if (shu == 15) // 如果蛇节数等于15则胜利
			{
				t = false;
				result = true;
				repaint();
			}

		}

	}

	public void paint(Graphics g) // 在面板上绘图
	{
		int x1 = this.getWidth() - 1;
		int y1 = this.getHeight() - 1;
		g.setColor(Color.RED);
		g.fillOval(weix, weiy, 10, 10);// 食物
		g.drawRect(0, 0, x1, y1); // 墙

		food.setBounds(weix, weiy, 10, 10);
		if (t == false) {
			if (result == false) {
				g.drawString("GAME OVER!", 250, 200);// 输出游戏失败
				for (Body b : snake) {
					b.setBackground(Color.GRAY);
				}
			} else
				g.drawString("YOU WIN!", 250, 200);// 输出游戏成功
		}
	}

	public void init() {
		// removeAll();
		shu = 0;
		x = 0;
		y = 0;
		t = true;
		result = true;
		snake.clear();
		thread = new Thread(this);

		Body b=new Body();
		b.setFocusable(true);
		snake.add(b);
		add(b);
		b.setBackground(Color.BLACK);
		b.addKeyListener(skl); // 为蛇头添加键盘监视器
		b.setBounds(0, 0, 10, 10);
		handler.direction = KeyEvent.VK_RIGHT;
		repaint();
		b.requestFocus();
		// requestFocus(true);
	}

	private void setLoc(int x, int y) throws InterruptedException {
		Body head = snake.get(0);
		head.setLocation(x, y);
		if (x == weix && y == weiy) {
			if(food instanceof Body){
			shu++;
			food.setBackground(Color.WHITE);
			food.setBounds(snake.get(shu - 1).getBounds());
			snake.add((Body) food);
			int	seed = (int) (Math.random() * 99);
			weix = (int) (seed * 0.1) * 60;
			weiy = (int) (seed % 10) * 40;
			food=new Body();
			add(food);
			repaint();
			}
		}
		Thread.sleep(100);
	}
}
