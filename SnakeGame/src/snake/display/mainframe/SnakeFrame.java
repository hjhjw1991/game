package snake.display.mainframe;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Frame;
import java.awt.Label;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * 贪吃蛇游戏 改自http://zhidao.baidu.com/link?url=
 * fGK0k_K_kh3EKsL3heZp6ufP2yP9MPf22nzsSOjVkqI7JXLVgZyzhEYa0CJcsPuEf2qev4wAX5aYfXd2dP_iW_
 *
 * @author HJ
 *
 */
public class SnakeFrame {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new MyWindow();

	}

}

class MyWindow extends Frame// 自定义窗口类
{
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	MyPanel my;
	Button begin, pause;
	Panel panel;

	MyWindow() {
		super("GreedySnake");
		my = new MyPanel();
		begin = new Button("begin");
		panel = new Panel();
		panel.add(begin);
		panel.add(new Label("开始游戏"));
		pause = new Button("pause");
		panel.add(pause);
		panel.add(new Label("暂停游戏"));
		panel.add(new Label("按上下左右键控制蛇行动"));
		add(panel, BorderLayout.NORTH);
		add(my, BorderLayout.CENTER);
		setBounds(100, 100, 610, 500);
		setVisible(true);
		validate();
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		MyActionListener ml = new MyActionListener();
		begin.addActionListener(ml);
		pause.addActionListener(ml);
	}

	class MyActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			Button source = (Button) e.getSource();
			if (source == begin) {
				if (my.thread != null) {
					my.thread = null;
				}
				my.init();
				my.thread.start(); // 开始线程
				my.validate();
			}
			if (source == pause) {

			}
		}
	}
}